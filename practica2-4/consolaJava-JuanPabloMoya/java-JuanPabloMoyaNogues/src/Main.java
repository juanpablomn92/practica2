import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner teclado=new Scanner(System.in);
		int opcion;
		do{
			System.out.println("\tMENU");
			System.out.println("1. Comprobar si un numero es primo");
			System.out.println("2. Comprobar si un numero es par");
			System.out.println("3. Introducir una cadena y mostrar la cadena invertida");
			System.out.println("4. Comprobar si dos cadenas son iguales");
			System.out.println("5. Salir");
			System.out.print("Introduce opcion (1-5)");
			opcion=teclado.nextInt();
			switch(opcion){
				case 1: numeroPrimo();
				break;
				case 2: numeroPar();
				break;
				case 3: invertirCadena();
				break;
				case 4: compararCadenas();
				break;
				case 5: System.exit(0);
				default: System.out.println("Error");
			}
		}
		while(opcion!=5);
	}

	private static void compararCadenas() {
		// TODO Auto-generated method stub
		Scanner teclado=new Scanner(System.in);
		String cadena1,cadena2;
		System.out.println("Introduce primera cadena");
		cadena1=teclado.nextLine();
		System.out.println("Introduce segunda cadena");
		cadena2=teclado.nextLine();
		if(cadena1.equalsIgnoreCase(cadena2)){
			System.out.println(cadena1+" = "+cadena2);
		}
		else{
			System.out.println(cadena1+" y "+cadena2+" no son iguales");
		}
		
	}

	private static void invertirCadena() {
		// TODO Auto-generated method stub
		Scanner teclado=new Scanner(System.in);
		String cadena, cadenaInvertida;
		System.out.println("Introduce cadena");
		cadena=teclado.nextLine();
		cadenaInvertida="";
        for(int i=(cadena.length()-1); i>=0; i--)
        {
            cadenaInvertida = cadenaInvertida + cadena.charAt(i);
        }
        System.out.println(cadenaInvertida);
	}

	private static void numeroPar() {
		// TODO Auto-generated method stub
		int numero;
		Scanner teclado=new Scanner(System.in);
		System.out.println("Introduce un numero");
		numero=teclado.nextInt();
		if(numero%2==0){
			System.out.println("El numero "+numero+" es par");
		}
		else{
			System.out.println("El numero "+numero+" no es par");
		}
	}

	private static void numeroPrimo() {
		// TODO Auto-generated method stub
		int numero;
		boolean primo=true;
		Scanner teclado=new Scanner(System.in);
		System.out.println("Introduce un numero");
		numero=teclado.nextInt();
		for(int i=2;i<numero;i++){
			if(numero%i==0){
				primo=false;
			}
		}
		if(primo==true){
			System.out.println("El numero "+numero+" es primo");
		}
		else{
			System.out.println("El numero "+numero+" no es primo");
		}
		
	}

}
