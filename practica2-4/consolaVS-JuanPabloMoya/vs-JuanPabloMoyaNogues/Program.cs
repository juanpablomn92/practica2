﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vsJuanPabloMoyaNogues
{

    class Program
    {
        static void Main(string[] args)
        {
            // TODO Auto-generated method stub
            
            int opcion;
            do
            {
                Console.WriteLine("\tMENU");
                Console.WriteLine("1. Comprobar si un numero es primo");
                Console.WriteLine("2. Comprobar si un numero es par");
                Console.WriteLine("3. Introducir una cadena y mostrar la cadena invertida");
                Console.WriteLine("4. Comprobar si dos cadenas son iguales");
                Console.WriteLine("5. Salir");
                Console.WriteLine("Selecciona opcion(1-5)");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        numeroPrimo();
                        break;
                    case 2:
                        numeroPar();
                        break;
                    case 3:
                        invertirCadena();
                        break;
                    case 4:
                        compararCadenas();
                        break;
                    case 5:
                        Console.WriteLine("Adios");
                        break;
                    default:
                        Console.WriteLine("Error");
                        break;

                }
            }
            while (opcion != 5);
            
            Console.ReadKey();

        }

        private static void numeroPrimo()
        {
            int numero;
            Boolean primo = true;
            Console.Write("Introduce un numero: ");
            numero = int.Parse(Console.ReadLine());
            for (int i = 2; i < numero; i++)
            {
                if (numero % i == 0)
                {
                    primo = false;
                }
            }
            if (primo == true)
            {
                Console.WriteLine("El numero " + numero + " es primo");
            }
            else
            {
                Console.WriteLine("El numero " + numero + " no es primo");
            }
        }

        private static void numeroPar()
        {
            int numero;
            Console.Write("Introduce un numero: ");
            numero = int.Parse(Console.ReadLine());
            if (numero % 2 == 0)
            {
                Console.WriteLine("El numero " + numero + " es par");
            }
            else
            {
                Console.WriteLine("El numero " + numero + " no es par");
            }
        }

        private static void invertirCadena()
        {
            String cadena, cadenaInvertida;
            Console.Write("Introduce cadena: ");
            cadena = Console.ReadLine();
            cadenaInvertida = "";
            for (int i = (cadena.Length - 1); i >= 0; i--)
            {
                cadenaInvertida = cadenaInvertida + cadena[i];
            }
            Console.WriteLine(cadenaInvertida);
        }

        private static void compararCadenas()
        {
            String cadena1, cadena2;
            Console.Write("Introduce primera cadena: ");
            cadena1 = Console.ReadLine();
            Console.Write("Introduce segunda cadena: ");
            cadena2 = Console.ReadLine();
            if (cadena1.Equals(cadena2))
            {
                Console.WriteLine(cadena1 + " = " + cadena2);
            }
            else
            {
                Console.WriteLine(cadena1 + " y " + cadena2 + " no son iguales");
            }
        }
    }
}

