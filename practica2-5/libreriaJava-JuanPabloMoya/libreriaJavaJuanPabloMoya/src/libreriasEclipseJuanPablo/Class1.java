package libreriasEclipseJuanPablo;

public class Class1 {
	public static void compararCadenas(String cadena1, String cadena2) {
		
		if(cadena1.equalsIgnoreCase(cadena2)){
			System.out.println(cadena1+" = "+cadena2);
		}
		else{
			System.out.println(cadena1+" y "+cadena2+" no son iguales");
		}
		
	}

	public static void invertirCadena(String cadena) {
		// TODO Auto-generated method stub
		String cadenaInvertida;
		cadenaInvertida="";
        for(int i=(cadena.length()-1); i>=0; i--)
        {
            cadenaInvertida = cadenaInvertida + cadena.charAt(i);
        }
        System.out.println(cadenaInvertida);
	}

	public static void numeroPar(int numero) {
		if(numero%2==0){
			System.out.println("El numero "+numero+" es par");
		}
		else{
			System.out.println("El numero "+numero+" no es par");
		}
	}
	
	public static void numeroMaximo(int numero1, int numero2, int numero3)
    {
        int maximo;
        if(numero1>numero2 && numero1 > numero3)
        {
            maximo = numero1;
        }
        else if (numero2 > numero1 && numero2>numero3)
        {
            maximo = numero2;
        }
        else
        {
            maximo = numero3;
        }
        System.out.println("El maximo es "+maximo);
    }

	public static void numeroPrimo(int numero) {
		boolean primo=true;
		for(int i=2;i<numero;i++){
			if(numero%i==0){
				primo=false;
			}
		}
		if(primo==true){
			System.out.println("El numero "+numero+" es primo");
		}
		else{
			System.out.println("El numero "+numero+" no es primo");
		}
		
	}
}
