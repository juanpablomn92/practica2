import libreriasEclipseJuanPablo.Class1;
import libreriasEclipseJuanPablo.Class2;

import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner teclado=new Scanner(System.in);
		String cadena1;
        String cadena2; 
        int numero1, numero2, numero3;
        System.out.print("Introduzca cadena1: ");
        cadena1 = teclado.nextLine();
        System.out.print("Introduzca cadena2: ");
        cadena2 = teclado.nextLine();
        System.out.print("Introduzca numero1: ");
        numero1 = teclado.nextInt();
        System.out.print("Introduzca numero2: ");
        numero2 = teclado.nextInt();
        System.out.print("Introduzca numero3: ");
        numero3 = teclado.nextInt();
        Class1.compararCadenas(cadena1,cadena2);
        Class1.invertirCadena(cadena1);
        Class1.numeroMaximo(numero1,numero2,numero3);
        Class1.numeroPar(numero1);
        Class1.numeroPrimo(numero1);
        Class2.sumar(numero1,numero2);
        Class2.restar(numero1, numero2);
        Class2.multiplicar(numero1, numero2);
        Class2.dividir(numero1, numero2);
        Class2.potencia(numero1, numero2);
	}

}
