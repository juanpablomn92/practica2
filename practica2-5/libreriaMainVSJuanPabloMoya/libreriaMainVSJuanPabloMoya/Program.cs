﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libreriaJuanPablo;

namespace libreriaMainVSJuanPabloMoya
{
    class Program
    {
        static void Main(string[] args)
        {
            String cadena1;
            String cadena2; 
            int numero1, numero2, numero3;
            Console.Write("Introduzca cadena1: ");
            cadena1 = Console.ReadLine();
            Console.Write("Introduzca cadena2: ");
            cadena2 = Console.ReadLine();
            Console.Write("Introduzca numero1: ");
            numero1 = int.Parse(Console.ReadLine());
            Console.Write("Introduzca numero2: ");
            numero2 = int.Parse(Console.ReadLine());
            Console.Write("Introduzca numero3: ");
            numero3 = int.Parse(Console.ReadLine());
            Class1.compararCadenas(cadena1,cadena2);
            Class1.invertirCadena(cadena1);
            Class1.numeroMaximo(numero1,numero2,numero3);
            Class1.numeroPar(numero1);
            Class1.numeroPrimo(numero1);
            Class2.sumar(numero1,numero2);
            Class2.restar(numero1, numero2);
            Class2.multiplicar(numero1, numero2);
            Class2.dividir(numero1, numero2);
            Class2.potencia(numero1, numero2);

            Console.ReadKey();
        }
    }
}
