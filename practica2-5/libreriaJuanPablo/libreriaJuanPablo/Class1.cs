﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libreriaJuanPablo
{
    public class Class1
    {
        public static void numeroPrimo(int numero)
        {
            Boolean primo = true;
            for (int i = 2; i < numero; i++)
            {
                if (numero % i == 0)
                {
                    primo = false;
                }
            }
            if (primo == true)
            {
                Console.WriteLine("El numero " + numero + " es primo");
            }
            else
            {
                Console.WriteLine("El numero " + numero + " no es primo");
            }
        }

        public static void numeroPar(int numero)
        {
            if (numero % 2 == 0)
            {
                Console.WriteLine("El numero " + numero + " es par");
            }
            else
            {
                Console.WriteLine("El numero " + numero + " no es par");
            }
        }

        public static void numeroMaximo(int numero1, int numero2, int numero3)
        {
            int maximo;
            if(numero1>numero2 && numero1 > numero3)
            {
                maximo = numero1;
            }
            else if (numero2 > numero1 && numero2>numero3)
            {
                maximo = numero2;
            }
            else
            {
                maximo = numero3;
            }
            Console.WriteLine("El maximo es " + maximo);
        }

        public static void invertirCadena(String cadena)
        {
            String cadenaInvertida;
            cadenaInvertida = "";
            for (int i = (cadena.Length - 1); i >= 0; i--)
            {
                cadenaInvertida = cadenaInvertida + cadena[i];
            }
            Console.WriteLine(cadenaInvertida);
        }

        public static void compararCadenas(String cadena1, String cadena2)
        {
            if (cadena1.Equals(cadena2))
            {
                Console.WriteLine(cadena1 + " = " + cadena2);
            }
            else
            {
                Console.WriteLine(cadena1 + " y " + cadena2 + " no son iguales");
            }
        }

    }
}
