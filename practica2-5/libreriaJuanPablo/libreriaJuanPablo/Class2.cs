﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libreriaJuanPablo
{
    public class Class2
    {
        public static void sumar(int numero1, int numero2)
        {
            int suma = numero1 + numero2;
            Console.WriteLine(numero1 + "+" + numero2 + "="+suma);
        }
        public static void restar(int numero1, int numero2)
        {
            int resta = numero1 + numero2;
            Console.WriteLine(numero1 + "-" + numero2 + "="+resta);
        }
        public static void multiplicar(int numero1, int numero2)
        {
            int multiplicacion = numero1 * numero2;
            Console.WriteLine(numero1 + "*" + numero2 + "="+multiplicacion);
        }
        public static void dividir(int numero1, int numero2)
        {
            int division = numero1 / numero2;
            Console.WriteLine(numero1 + "/" + numero2 + "="+division);
        }

        public static void potencia(int numero1, int numero2)
        {
            int potencia = 1;
            for(int i=0; i<numero2; i++)
            {
                potencia = potencia * numero1;
            }
            Console.WriteLine(numero1 + "^" + numero2 + "="+potencia);
        }
    }
}
