/**
 * @author JuanPablo
 * @since 11/01/2018
 */

package interfacesGraficas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;

public class interfaz extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					interfaz frame = new interfaz();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public interfaz() {
		setTitle("Creador de jugadores y entrenadores para tu equipo");
		setIconImage(Toolkit.getDefaultToolkit().getImage(interfaz.class.getResource("/imagen/26bd.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 560, 345);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenu mnNuevoJugador = new JMenu("Crear");
		mnArchivo.add(mnNuevoJugador);
		
		JMenuItem mntmJugador = new JMenuItem("Jugador");
		mnNuevoJugador.add(mntmJugador);
		
		JMenuItem mntmEntrenador = new JMenuItem("Entrenador");
		mnNuevoJugador.add(mntmEntrenador);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar ");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmCargarJugador = new JMenuItem("Cargar....");
		mnArchivo.add(mntmCargarJugador);
		
		JMenu mnEdicin = new JMenu("Edici\u00F3n");
		menuBar.add(mnEdicin);
		
		JMenuItem mntmRehacer = new JMenuItem("Rehacer");
		mnEdicin.add(mntmRehacer);
		
		JMenuItem mntmDeshacer = new JMenuItem("Deshacer");
		mnEdicin.add(mntmDeshacer);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 13, 56, 16);
		contentPane.add(lblNombre);
		
		textField = new JTextField();
		textField.setBounds(67, 10, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Apellidos");
		lblNewLabel.setBounds(12, 53, 56, 16);
		contentPane.add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setBounds(67, 50, 116, 22);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(342, 97, 47, 22);
		contentPane.add(spinner);
		
		JLabel lblDorsal = new JLabel("Dorsal");
		lblDorsal.setBounds(285, 100, 56, 16);
		contentPane.add(lblDorsal);
		
		JLabel lblPosicin = new JLabel("Posici\u00F3n");
		lblPosicin.setBounds(285, 13, 56, 16);
		contentPane.add(lblPosicin);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {" ", "Portero", "Defensa", "Centrocampista", "Delantero"}));
		comboBox.setBounds(342, 10, 110, 22);
		contentPane.add(comboBox);
		
		JLabel lblCapitn = new JLabel("Capit\u00E1n");
		lblCapitn.setBounds(285, 53, 56, 16);
		contentPane.add(lblCapitn);
		
		JCheckBox checkBox = new JCheckBox("");
		checkBox.setBounds(339, 49, 113, 25);
		contentPane.add(checkBox);
		
		JLabel lblColorDorsal = new JLabel("Color dorsal");
		lblColorDorsal.setBounds(285, 132, 91, 16);
		contentPane.add(lblColorDorsal);
		
		JRadioButton rdbtnRojo = new JRadioButton("Blanco");
		rdbtnRojo.setBounds(285, 157, 72, 25);
		contentPane.add(rdbtnRojo);
		
		JRadioButton rdbtnNegro = new JRadioButton("Negro");
		rdbtnNegro.setBounds(361, 157, 127, 25);
		contentPane.add(rdbtnNegro);
		
		JSlider slider = new JSlider();
		slider.setValue(0);
		slider.setMinimum(150);
		slider.setMaximum(200);
		slider.setMajorTickSpacing(10);
		slider.setToolTipText("");
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setBounds(12, 192, 200, 80);
		contentPane.add(slider);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(12, 176, 56, 16);
		contentPane.add(lblAltura);
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(interfaz.class.getResource("/imagen/26bd.png")));
		label_2.setBounds(80, 85, 78, 82);
		contentPane.add(label_2);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setForeground(Color.BLACK);
		btnEnviar.setBackground(Color.RED);
		btnEnviar.setBounds(279, 219, 97, 25);
		contentPane.add(btnEnviar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setForeground(Color.BLACK);
		btnCancelar.setBackground(Color.RED);
		btnCancelar.setBounds(407, 219, 97, 25);
		contentPane.add(btnCancelar);
	}
}
