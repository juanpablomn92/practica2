package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		//Creamos variables
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		//damos valor a las variables
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		//Cuenta el numero de divisores que tiene el numero que hemos introducido
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		//si el numero de divisores es mayor que 2 nos pinta "No lo es", en caso contrario nos pintar� "Si lo es"
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
